grails.servlet.version = "3.0" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.work.dir = "target/work"
grails.project.target.level = 1.7
grails.project.source.level = 1.7
//grails.project.war.file = "target/${appName}-${appVersion}.war"

grails.project.dependency.resolver = "maven" // or ivy
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {}
    log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve

    def gebVersion = "0.12.1"
    def seleniumVersion = "2.45.0"

    repositories {
        inherits true // Whether to inherit repository definitions from plugins
        grailsPlugins()
        grailsHome()
        grailsCentral()
        mavenCentral()
        mavenLocal()
        mavenRepo "https://oss.sonatype.org/content/repositories/releases"
        mavenRepo "http://projects.k-int.com/nexus-webapp-1.4.0/content/repositories/releases"
        mavenRepo "http://jaspersoft.artifactoryonline.com/jaspersoft/third-party-ce-artifacts/"
        mavenRepo "http://jasperreports.sourceforge.net/maven2/com/lowagie/itext/2.1.7.js2/"
        mavenRepo "http://central.maven.org/maven2/"
    }

    dependencies {
        compile 'org.codehaus.groovy:groovy-backports-compat23:2.3.5'
        build('org.grails:grails-docs:2.3.11') {
            excludes 'itext'
        }

        compile 'commons-codec:commons-codec:1.6'

        test("org.seleniumhq.selenium:selenium-htmlunit-driver:$seleniumVersion") {
            exclude 'xml-apis'
        }
        test "org.seleniumhq.selenium:selenium-firefox-driver:$seleniumVersion"
        test "org.seleniumhq.selenium:selenium-support:$seleniumVersion"
        test "org.spockframework:spock-grails-support:0.7-groovy-2.0"
        test "org.gebish:geb-spock:$gebVersion"
    }

    plugins {

        compile ":h2:0.2.6"
        runtime ":resources:1.2.8"
        runtime ':fields:1.3'
        compile ":scaffolding:2.0.3"

        build ':tomcat:7.0.54'
        runtime ":database-migration:1.4.0"
        compile ':cache:1.1.7'

        test ":spock:0.7", {
            exclude "spock-grails-support"
        }
        test ":geb:$gebVersion"
        test ":remote-control:2.0"

        // Font awesome for font based icons.
        compile ":font-awesome-resources:4.3.0.1"

        runtime ":gsp-resources:0.4.4"
        runtime ":jquery:1.9.1"
    }
}
