modules = {
    application {
        dependsOn 'jquery'
    }

    sky {
        dependsOn 'jquery'
        resource url: 'js/modernizr.js'
        resource url: 'js/foundation.min.js', disposition: 'defer'
        resource url: 'js/jquery.dataTables.min.js'
        resource url: 'css/jquery.dataTables.css'
        resource url: 'css/foundation.min.css'
        resource url: 'css/main.css'
        dependsOn 'font-awesome'
    }
}