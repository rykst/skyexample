<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Welcome to Sky : Billing </title>
	</head>
	<body>


	<hr>

	<div id="summaryWrapper" class="row large-12">
		<div class="column medium-5 large-5">
			<div class="panel callout radius">
				<h5>Summary</h5>
				<div id="summaryContent"></div>
			</div>
		</div>
		<div class="column medium-5 large-5">
			<div class="panel callout radius">
				<h5>Mini Summary</h5>
				<div id="miniContent"></div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="medium-12 large-12 columns">

			<ul class="accordion" data-accordion>
				<li class="accordion-navigation">
					<a href="#panel1a">Itemised Telephone Billing</a>
					<div id="panel1a" class="content active">
						<table id="itemisedTable" class="display dataTable skyData" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>called</th>
									<th>duration</th>
									<th>cost</th>
								</tr>
							</thead>
							<tfoot>
							<tr>
								<th>called</th>
								<th>duration</th>
								<th>cost (£)</th>
							</tr>
							</tfoot>
							<tbody>
							</tbody>
						</table>
					</div>
					<br/>
				</li>
				<li class="accordion-navigation">
					<a href="#panel2a">Sky Package(s)</a>
					<div id="panel2a" class="content">
						<table id="packageTable" class="display dataTable skyData" cellspacing="0" width="100%">
							<thead>
							<tr>
								<th>called</th>
								<th>duration</th>
								<th>cost</th>
							</tr>
							</thead>

							<tfoot>
							<tr>
								<th>called</th>
								<th>duration</th>
								<th>cost (£)</th>
							</tr>
							</tfoot>

							<tbody>
							</tbody>
						</table>
					</div>
					<br/>
				</li>
				<li class="accordion-navigation">
					<a href="#panel3a">Sky Store</a>
					<div id="panel3a" class="content">
						<table id="skystoreTable" class="display dataTable skyData" cellspacing="0" width="100%">
							<thead>
							<tr>
								<th>Title</th>
								<th>Type</th>
								<th>cost (£)</th>
							</tr>
							</thead>
							<tfoot>
							<tr>
								<th>Title</th>
								<th>Type</th>
								<th>cost (£)</th>
							</tr>
							</tfoot>
							<tbody>
							</tbody>
						</table>
					</div>
					<br/>
				</li>
			</ul>

		</div>
	</div>



	<hr>

	<div id="prevMonths" class="row">
		<h4>Previous Bills... </h4>
		<div class="medium-12 large-12 columns">
			<div class="large-3 columns">
				<img src="http://placehold.it/200x175&text=[April]">
			</div>
			<div class="large-3 columns">
				<img src="http://placehold.it/200x175&text=[May]">
			</div>
			<div class="large-3 columns">
				<img src="http://placehold.it/200x175&text=[June]">
			</div>
			<div class="large-3 columns">
				<img src="http://placehold.it/200x175&text=[July]">
			</div>
		</div>
	</div>

	<div id="helpModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<h2 id="modalTitle">Quick Info</h2>
		<p>Content is separated into your itemised telephone bill, Sky Package, and Sky Store</p>
		<p>Each is divided into a section, which can be displayed or hidden on click to quickly see the information your interested in</p>
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	</div>
	</body>
</html>

<r:script type="text/javascript">
	var SkyApp = {
		cache: {},
		dataURL: 'http://safe-plains-5453.herokuapp.com/bill.json', //mocked data source

		onReady: function () {
			if(!SkyApp.cache[SkyApp.dataURL.hash])
			{
				$.ajax({
					method: 'GET',
					cache:true,
					url: SkyApp.dataURL,
					dataType: 'JSON' //convert for me please :)
				}).done(function(data) {
					SkyApp.processResults(data);
				}); //callback for processing
			}
		},

		processResults: function (ajax) {
			if(ajax) {
				SkyApp.summary(ajax);
				SkyApp.setupTable(ajax);
			} else
				alert('Unable to process customer data, please reload to try again');
		},

		summary: function(data) {
			var html = "<p>";
			if(data.statement.generated)
				html+=	"Generated Date:"+data.statement.generated+"</br>";
			if(data.statement.due)
				html+=	"Due Date:"+data.statement.due+"</br>";
			if(data.period)
				html+=	"Billing Period:"+data.period.from+" to "+data.period.to+"</br>";
			if(data.total)
				html+=	"Total :<b>"+data.total+"</b>";
			html += "</p>";
			$("#summaryContent").html(html);
		},

		miniSummary: function(data) {
			$('#miniContent').html(data);
		},

		//Bad practice, would normally be templated
		setupTable: function (data) {
			var calls = data.callCharges.calls;
			var pkg   = data.package.subscriptions;
			var store = data.skyStore;
			var miniSummary = ""
			if(calls)
			{
				miniSummary = "<p>No of calls: "+calls.length+" Total: £"+data.callCharges.total+"</p>"
				var row = "";
				for (var i = 0; i < calls.length; i++)
				{
					row+="<tr id='call_"+i+"'>";
					row += "<td data-search='"+ calls[i].called + "'>" + calls[i].called + " </td>";
					row += "<td>"+ calls[i].duration + "</td>";
					row += "<td>" + calls[i].cost + "</td>";
					row+="</tr>";
				}
				$('#itemisedTable tbody').append(row);

				miniSummary += "<p>No of packages: "+pkg.length+" Total: £"+data.package.total+"</p>"
				row = "";
				for (var i = 0; i < pkg.length; i++)
				{
					row+="<tr id='pkg_"+i+"'}>";
					row += "<td data-search='"+ pkg[i].name + "'>" + pkg[i].name + " </td>";
					row += "<td data-search='"+ pkg[i].type + "'>" + pkg[i].type + "</td>";
					row += "<td>" + pkg[i].cost + "</td>";
					row+="</tr>";
				}
				$('#packageTable tbody').append(row);

				miniSummary += "<p>No of purchases: "+(store.buyAndKeep.length + store.rentals.length)+" Total: £"+data.package.total+"</p>";
				row = "";
				for (var i = 0; i < store.buyAndKeep.length; i++)
				{
					row+="<tr id='storeBK_"+i+"'}>";
					row += "<td data-search='"+ store.buyAndKeep[i].title + "'>" + store.buyAndKeep[i].title + " </td>";
					row += "<td data-search='buy and keep'>Buy and Keep</td>";
					row += "<td>" + store.buyAndKeep[i].cost + "</td>";
					row+="</tr>";
				}
				for (var i = 0; i < store.rentals.length; i++)
				{
					row+="<tr id='storeR_"+i+"'}>";
					row += "<td data-search='"+ store.rentals[i].title + "'>" + store.rentals[i].title + " </td>";
					row += "<td data-search='rentals'>Rentals</td>";
					row += "<td>" + store.rentals[i].cost + "</td>";
					row+="</tr>";
				}
				$('#skystoreTable tbody').append(row);
			}
			SkyApp.miniSummary(miniSummary);
			$('.dataTable.skyData').dataTable({});
		}

	};
	$(document).foundation().ready(SkyApp.onReady);
</r:script>
