<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
	<r:require modules="sky"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><g:layoutTitle default="Sky"/></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
	<g:layoutHead/>
	<r:layoutResources />
</head>
<body>
<div class="row">
	<div class="large-12 columns">
		<div id="skyWrapper"><a href="#" class="skyLogo"></a></div>
		<h1 class="">My Bills: August</h1>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<div class="sticky">
			<nav class="top-bar" data-topbar="" role="navigation" data-options="sticky_on: large">
				<ul class="title-area">
					<!-- Title Area -->
					<li class="name">
						<h1><a href="#">Home</a></h1>
					</li>
					<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
					<li class="toggle-topbar menu-icon"><a id="help" href="#"><span>Menu</span></a></li>
				</ul>


				<section class="top-bar-section">

					<!-- Right Nav Section -->
					<ul class="right">
						<li class="divider hide-for-small"></li>
						<li><a class="icon-sky"  data-reveal-id="helpModal" data-title="Click me for helpful info" href="#">Help</a></li>
					</ul>
				</section></nav>
		</div>
	</div>
</div>

<div id="breadcrumbWrapper" class="row">
	<br/>
	<div class="column large-5">
		<ul class="breadcrumbs">
			<li class="unavailable"><a href="#">Home</a></li>
			<li class="unavailable"><a href="#">My Sky</a></li>
			<li class="current"><a href="#">Bills & Payments</a></li>
		</ul>
	</div>
</div>

<g:layoutBody/>
<div class="zurb-footer-top bg-grey">
	<hr>
	<div class="row property">
		<div class="medium-12 columns">
			<div class="row collapse">
				<div class="medium-3 columns">
					<div class="learn-links">
						<h4 class="hide-for-small">Explore Sky</h4>
						<ul class="footerLinks">
							<li><a href="http://business.sky.com/">Sky Corporate</a></li>
							<li><a href="http://business.sky.com/">Sky For Businesses</a></li>
							<li><a href="http://communaltv.sky.com/">Sky Communal TV</a></li>
							<li><a href="http://www.sky.com/shop/store-locator/">Store Locator</a></li>
							<li><a href="https://www.workforsky.com/">Work For Sky</a></li>
							<li><a href="http://www.sky.com/products/broadband-talk/sky-broadband-shield/safety-centre">Safety Centre</a></li>
							<li><a href="http://www.skymedia.co.uk/">Advertise With Us</a></li>
						</ul>
					</div>
				</div>
				<div class="medium-3 columns">
					<div class="support-links">
						<h4 class="hide-for-small">Sky Initiatives</h4>
						<ul class="footerLinks">
							<li><a href="">Bigger Picture</a></li>
							<li><a href="">Sky Academy</a></li>
							<li><a href="">Sky Sports Living For Sport</a></li>
							<li><a href="">Sky Academy Starting Out</a></li>
							<li><a href="">Sky Ride</a></li>
							<li><a href="">Sky Rainforest Rescue</a></li>
							<li><a href="">Advertise With Us</a></li>
						</ul>
					</div>
				</div>
				<div class="medium-3 columns">
					<div class="connect-links">
						<h4 class="hide-for-small">Sky Channels</h4>
						<ul class="footerLinks">
							<li><a href="">Sky1 </a></li>
							<li><a href="">Sky Atlantic</a></li>
							<li><a href="">Sky Living</a></li>
							<li><a href="">Sky Arts</a></li>
							<li><a href="">Sky Movies</a></li>
							<li><a href="">Sky Sports</a></li>
							<li><a href="">Sky News</a></li>
						</ul>
					</div>
				</div>
				<div class="medium-3 columns">
					<div class="connect-links">
						<h4 class="hide-for-small">More Sky Sites</h4>
						<ul class="footerLinks">
							<li><a href="">NOW TV</a></li>
							<li><a href="">Sky Tickets</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
</div>


<r:layoutResources />


</body>
</html>
