# README #

### Pre-req ###
* OS Linux Flavours or MAC OSX, and Windows 
* You will need Grails on your system: 2.3.11
* GVM Tool (useful for groovy, grails, etc)


### Quick run script (Linux & Mac only) ###
Inside root of project directory, will need to pull project firstly **sh setupSkyApp.sh**

```
#!bash

#!/bin/bash

echo "Running setup... downloading gvmtool"
curl -s -o gvmInstall.sh get.gvmtool.net

echo "checking Installing ..."
chmod +x gvmInstall.sh
yes | sh gvmInstall.sh
source "${GVM_DIR}/bin/gvm-init.sh"

echo "attempting installation of grails version 2.3.11"
yes | gvm install grails 2.3.11

echo 'About to run app.. On port 8093'
echo ' run the following to quickly kill : kill $(lsof -t -i:8093)'

grails run-app -Dserver.port=8093

```



### OS Linux flavour/Mac OSX ###
* git clone https://rykst@bitbucket.org/rykst/skyexample.git
* curl -s get.gvmtool.net | bash 
* gvm u grails 2.3.11
* grails run-app -Dserver.port=8093
* Run functional tests : grails test-app functional: -Dserver.port=8090 **NOTE** (Testing source : skyGrails/test/functional)

     	
### Windows… Do developers even use windows :P ###
* Powershell
* [Install-Module posh-gvm](https://github.com/flofreud/posh-gvm)
* Import-Module posh-gvm
* gvm u grails 2.3.11
* grails run-app -Dserver.port=8093 
* grails -Dgeb.env=windows test-app functional: