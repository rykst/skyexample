import geb.spock.GebReportingSpec
import pages.IndexPage


class BasicSpec extends GebReportingSpec {

    def "search 'sky' in duckduckgo"() {
        given: "we are on the duckduckgo search-engine"
        go "https://duckduckgo.com"

        when: "we search for 'sky'"
        $("#search_form_homepage").q = "sky"
        $("#search_button_homepage").click()

        then: "the first result is the Sky website"
        assert $("#links").find(".links_main a", 0).attr("href") == "http://www.sky.com/"
    }

    def "should have 3 data-tables with non-empty list"() {
        when: "We are on the Index page"
        to IndexPage

        then: "Tables should each contain 10 rows as with standard pagination max size"
        assert $("table", 0..2).each {it.find("tr").size() == [10, 10, 10]}
    }

    def "should have 3 data-tables with specific titles"() {
        when: "We are on the Index page"
        at IndexPage

        then: "Check titles match, i.e. 'Itemised Telephone Billing', 'Sky Package(s)', 'Sky Store'"
        assert $(".accordion-navigation", 0..2).children("a")*.text() == ["Itemised Telephone Billing", "Sky Package(s)", "Sky Store"]
    }

    def "Filter tables with terms '0207' (size:10, not incl paginated)"() {
        when: "We are on the Index page"
        at IndexPage
        $("#itemisedTable_filter",0).find("input").value("0207")


        then: "Tables should contain 10, 1, 2 rows"
        assert $("#itemisedTable_filter",0).find("input").value() == "0207"
        assert $("table",0).find("tbody tr").size() == 10
    }
}
