package pages

import geb.Page

class IndexPage extends Page {
    static url = "/skyGrails"
    static at = { browser.page.title.startsWith "Welcome to Sky" };

    static content = {
        heading { $("h1").text() }
    }
}
