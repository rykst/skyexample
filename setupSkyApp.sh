#!/bin/bash

echo "Running setup... downloading gvmtool"
curl -s -o gvmInstall.sh get.gvmtool.net

echo "checking Installing ..."
chmod +x gvmInstall.sh
yes | sh gvmInstall.sh
source "${GVM_DIR}/bin/gvm-init.sh"

echo "attempting installation of grails version 2.3.11"
yes | gvm install grails 2.3.11

echo 'About to run app.. On port 8093'
echo ' run the following to quickly kill : kill $(lsof -t -i:8093)'

grails run-app -Dserver.port=8093
